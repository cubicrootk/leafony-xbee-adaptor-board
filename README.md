
# Leafony・XBeeソケット 連携ボード の紹介

XBeeファミリーのRFモジュールは、ZigBee, LoRa, BLE, WiFi等の通信をサポートしているが、他社からXBeeソケット形状互換の製品も出ている。Leafonyとしても、既にBLE、WiFi、LTE等の製品がある。

![Leafony・XBeeソケット連携ボード](images/Leafony-XBee-Adapter_Overview.png)

複数の無線を組み合わせる応用分野があるので、「Leafony・XBeeソケット 連携ボード」を作成しました。

- 例１: 圃場でメッシュネットワークでセンサー情報を集めエッジ・コンピューティング処理し、更に、LTE通信してクラウド・コンピューティングと連携する
- 例２: UAV/ ドローンの無線通信を、規格・周波数帯の異なる無線にて二重化したい（Safety）

## Leafony XBeeソケット 連携ボード: 設計目標

- LeafonyシステムからXBee製品を利用する
  - DIGI社のXBee3: ZigBee, LoRa, BLE等で通信する

    ![Leafony・XBeeソケット連携ボード: XBee装着例](images/Leafony-XBee-Adapter_XBee.png)

- XBeeソケット形状のモデムを利用する
  - LTE-M モデム
- XBeeソケット形状のGNSS受信機を利用する
  - ArduSimple社のSimpleRTK2B-lite

    ![Leafony・XBeeソケット連携ボード: GNSS装着例](images/Leafony-XBee-Adapter_SimpleRTK2BL.png)

- 将来拡張案
  - CAN通信を追加する
  - XBeeソケット互換製品の試作用途

## Leafony XBeeソケット 連携ボード: PCB 概観

プリント基板（PCB）は、XBee互換ソケット、Leafonyバス、LED、電源選択回路、コネクタで構成

   ![Leafony・XBeeソケット連携 PCB](images/Leafony-XBee-Adapter_PCB.png)

## Leafony XBeeソケット 連携ボード: 回路図 (1/3)

LeafonyバスとXBeeソケットの信号線の接続についは、下記の表を参照（回路図より見易い）

   ![Leafony・XBeeソケット連携 回路図１](images/Leafony-XBee-Adapter_signals.png)

(補足: 本基板の下面のコネクタは、将来の拡張用でXBeeソケット製品を試作評価する目的です)

## Leafony XBeeソケット 連携ボード: 回路図 (2/3)

コネクタとでCAN信号について、下記の回路図に示します。

   ![Leafony・XBeeソケット連携 回路図２](images/Leafony-XBee-Adapter_connectors.png)

(補足: CANトランシーバの部品入手難で未評価)

## Leafony XBeeソケット 連携ボード: 回路図 (3/3)

電源選択回路を下記の回路図に示します。

   ![Leafony・XBeeソケット連携 回路図２](images/Leafony-XBee-Adapter_power-selectors.png)

Ideal Diode で 電源を選択します。テレメトリーコネクタは５V電源で3.3V信号線。CANコネクターは５V電源で、CAN信号は差動信号です。テレメトリーとCANポートは排他使用を前提としています。TSV等の対策部品を実装出来ななったので、活線挿抜はしない方が良い。

XBeeソケットには3.3V電源を供給します。LTEモデムの品種によっては3.3Vより高圧電源が必要なので、可変電圧の三端子レギュレーターも実装可能。
